﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bludiste
{   
    public partial class Form1 : Form
    {
        public Graphics graphics;
        public Bitmap bmp;

        public bludiste moje_krasne_blud;
        prisera karel;

        public Form1()
        {
            InitializeComponent();
            bmp = new Bitmap(panel1.Width, panel1.Height);
            graphics = Graphics.FromImage(bmp);

            graphics.Clear(Color.White);
            this.KeyPreview = true;
            nakresliDoPanelu();
        }


        private void nakresliDoPanelu()
        {
            panel1_Paint(this, null);
        }

        private void smaz()
        {
            graphics.Clear(Color.White);
            nakresliDoPanelu();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if (bmp == null)
            {
                return;
            }

            Graphics grp = panel1.CreateGraphics();
            int whiteSpaceLeft = 0;
            int whiteSpaceTop = 0;
            double aspectRatioSrc = (double)bmp.Width / bmp.Height;
            double aspectRatioDst = (double)panel1.Width / panel1.Height;
            if (aspectRatioSrc > aspectRatioDst)
            {
                whiteSpaceTop = panel1.Height - (int)(panel1.Width / aspectRatioSrc);
                whiteSpaceTop >>= 1;

            }
            else
            {
                whiteSpaceLeft = panel1.Width - (int)(panel1.Height * aspectRatioSrc);
                whiteSpaceLeft >>= 1;
            }
            Rectangle rSource = new Rectangle(0, 0, bmp.Width, bmp.Height);
            Rectangle rDestination = new Rectangle(whiteSpaceLeft, whiteSpaceTop, panel1.Width
            - (whiteSpaceLeft << 1), panel1.Height - (whiteSpaceTop << 1));
            grp.DrawImage(bmp, rDestination, rSource, GraphicsUnit.Pixel);


            grp.Dispose();
        }

        private void ulozBmp()
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = string.Empty;
            sfd.Filter = "Portable graphics *.png|*.png|Windows Bitmap *.bmp|*.bmp|Jpeg *.jpg|*.jpg";
            if (sfd.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }
            try
            {
                bmp.Save(sfd.FileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case 'w':
                    karel.jdi(0, -1);
                    break;
                case 'a':
                    karel.jdi(-1, 0);
                    break;
                case 's':
                    karel.jdi(0, 1);
                    break;
                case 'd':
                    karel.jdi(1, 0);
                    break;
            }
            nakresliDoPanelu();
            if (moje_krasne_blud.bunkaAt(moje_krasne_blud.cil) == moje_krasne_blud.bunkaAt(karel.pozice))
            {
                if (DialogResult.Yes == MessageBox.Show(
                                        "Dorazili jste do cíle, chcete vygenerovat další bludiště?",
                                        "Jste v cíli!",
                                        MessageBoxButtons.YesNo))
                {
                    button1_Click(sender, e);
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            smaz();
            int rozmer = 10;
            switch (comboBox1.SelectedIndex)
            {
                case -1:
                case 0: rozmer = 10;
                    break;
                case 1: rozmer = 15;
                    break;
                case 2: rozmer = 25;
                    break;
                case 3: rozmer = 50;
                    break;
                case 4: rozmer = 100;
                    break;
            }

            moje_krasne_blud = new bludiste(graphics, rozmer, rozmer, bmp.Height, bmp.Width);
            moje_krasne_blud.vybourejCestu();
            moje_krasne_blud.vykresliSe();
            karel = new prisera(moje_krasne_blud, Color.Green, new Point(0, 0));


            karel.vykresliSe();
            nakresliDoPanelu();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ulozBmp();
        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            panel1.Width = this.Width - 40;
            panel1.Height = this.Height - 76;
            bmp = new Bitmap(panel1.Width, panel1.Height);
            graphics = Graphics.FromImage(bmp);
            graphics.Clear(Color.White);
            nakresliDoPanelu();

            if (moje_krasne_blud != null)
            {
                moje_krasne_blud.zmenRozmer(graphics, panel1.Width, panel1.Height);
                moje_krasne_blud.vykresliSe();
            }
            if (karel != null)
            {
                karel.vykresliSe();
            }

            nakresliDoPanelu();
        }
    }

    public class bunka
    {

        static int pocet_bunek = 0;
        public static Pen pero = new Pen(Color.Black, 1);

        int barva;

        public bool zapadni_stena;
        public bool severni_stena;

        public bool navstivena;

        public static int vyska { get; set; }
        public static int sirka { get; set; }

        public int x;
        public int y;

        public Point pozice; //souradnice leveho horniho rohu

        public bunka(Point pozice, int vyska, int sirka, int barva, int x, int y)
        {
            this.pozice = pozice;
            this.barva = barva;
            this.x = x;
            this.y = y;

            pocet_bunek++;

            zapadni_stena = true;
            severni_stena = true;
            navstivena = false;
        }

        public bunka(Point pozice, int x, int y) :
            this(pozice, vyska, sirka, pocet_bunek, x, y) { }



        public void vykresliSe(Graphics dest, Color barva, Point kde)
        {

            if (zapadni_stena)
                dest.DrawLine(pero, kde.X, kde.Y, kde.X, kde.Y + vyska);
            if (severni_stena)
                dest.DrawLine(pero, kde.X, kde.Y, kde.X + sirka, kde.Y);
        }

        public void vykresliSe(Graphics dest, Color barva)
        {
            vykresliSe(dest, barva, this.pozice);
        }


        public void vykresliSe(Graphics dest)
        {
            vykresliSe(dest, Color.Black, this.pozice);
        }

        public void vybarviSe(Graphics dest, Color barva)
        {
            Brush st = new SolidBrush(barva);
            dest.FillRectangle(st, this.pozice.X, this.pozice.Y, sirka, vyska);
        }


    }

    public class bludiste
    {
        public bunka[,] matice;

        public Graphics graphics;


        public int vyska { get; set; }
        public int sirka { get; set; }

        public int vyska_pix { get; set; }
        public int sirka_pix { get; set; }

        public Point cil;

        Random gen1 = new Random();
        Random gen2 = new Random();

        /// <summary>
        /// Vygeneruje plne bludiste se vsemi stenami, ktere zabere vyska_pix * sirka_x pixelů a bude mít
        /// sirka * vyska bunek.
        /// </summary>
        /// <param name="dest"></param> graphics kam se bude bludiste vykreslovat
        /// <param name="sirka"></param> pocet bunek v radku
        /// <param name="vyska"></param> pocet bunek ve sloupci
        /// <param name="vyska_pix"></param> vyska bludiste v pixelech
        /// <param name="sirka_pix"></param> sirka bludiste v pixelech
        public bludiste(Graphics dest, int sirka, int vyska, int vyska_pix, int sirka_pix)
        {
            this.graphics = dest;
            this.vyska = vyska;
            this.sirka = sirka;
            this.cil = new Point(vyska - 1, sirka - 1);

            bunka.sirka = sirka_pix / sirka;
            bunka.vyska = vyska_pix / vyska;

            matice = new bunka[sirka, vyska];

            for (int i = 0; i < sirka; i++)
            {
                for (int j = 0; j < vyska; j++)
                {
                    matice[i, j] = new bunka(new Point(bunka.sirka * i, bunka.vyska * j), i, j);
                }
            }

            this.sirka_pix = sirka * bunka.sirka;
            this.vyska_pix = vyska * bunka.vyska;


        }

        /// <summary>
        /// Vygeneruje v plnem bludisti cesty
        /// </summary>
        public void vybourejCestu()
        {
            bunka tmpBunka, tmpSoused;
            Stack<bunka> zas = new Stack<bunka>();

            int rnX = (int)Math.Floor(sirka * gen1.NextDouble());
            int rnY = (int)Math.Floor(sirka * gen1.NextDouble());
            zas.Push(bunkaAt(rnX, rnY));

            while (zas.Count > 0)
            {
                tmpBunka = zas.Peek();
                tmpBunka.navstivena = true;
                tmpSoused = nahodnySoused(tmpBunka);
                if (tmpSoused != null)
                {
                    zbourejZed(tmpBunka, tmpSoused);
                    zas.Push(tmpSoused);
                }
                else
                {
                    zas.Pop();
                }

            }
        }

        private void zbourejZed(bunka tmpBunka, bunka tmpSoused)
        {
            if (tmpSoused.x - tmpBunka.x < 0)
            {
                tmpBunka.zapadni_stena = false;
                return;
            }
            if (tmpSoused.x - tmpBunka.x > 0)
            {
                tmpSoused.zapadni_stena = false;
                return;
            }
            if (tmpSoused.y - tmpBunka.y < 0)
            {
                tmpBunka.severni_stena = false;
                return;
            }
            if (tmpSoused.y - tmpBunka.y > 0)
            {
                tmpSoused.severni_stena = false;

                return;
            }
        }

        /// <summary>
        /// Vrati nahodneho, dosud nenavstiveneho souseda bunky b nebo null,
        /// pokud takovy neni.
        /// </summary>
        /// <param name="b">bunka, pro kterou se bude soused hledat</param>
        /// <returns>nahodny dosud nenavstiveny soused nebo null pokud neexistuje</returns>
        public bunka nahodnySoused(bunka b)
        {
            bunka[] sousedi = new bunka[4];
            int p1, p2, sousedu = 0;
            p2 = 0;
            for (p1 = -1; p1 < 2; p1 += 2)
            {
                if (jeUvnitrBlud(b.x + p1, b.y + p2))
                {
                    if (!bunkaAt(b.x + p1, b.y + p2).navstivena)
                    {
                        sousedi[sousedu] = bunkaAt(b.x + p1, b.y + p2);
                        sousedu++;
                    }
                }
            }

            p1 = 0;
            for (p2 = -1; p2 < 2; p2 += 2)
            {
                if (jeUvnitrBlud(b.x + p1, b.y + p2))
                {
                    if (!bunkaAt(b.x + p1, b.y + p2).navstivena)
                    {
                        sousedi[sousedu] = bunkaAt(b.x + p1, b.y + p2);
                        sousedu++;
                    }
                }
            }
            if (sousedu == 0)
            {
                return null;
            }
            int k = (int)Math.Floor(gen2.NextDouble() * sousedu);
            return sousedi[k];
        }

        /// <summary>
        /// vykresli aktualni bludiste do graphics
        /// </summary>
        public void vykresliSe()
        {

            bunkaAt(cil).vybarviSe(graphics, Color.YellowGreen);

            for (int i = 0; i < sirka; i++)
            {
                for (int j = 0; j < vyska; j++)
                {
                    matice[i, j].vykresliSe(this.graphics);
                }
            }

           

            this.graphics.DrawLine(bunka.pero, 0, vyska_pix - 1, sirka_pix - 1, vyska_pix - 1);
            this.graphics.DrawLine(bunka.pero, sirka_pix - 1, 0, sirka_pix - 1, vyska_pix - 1);

        }

        public bunka bunkaAt(Point poz)
        {
            return matice[poz.X, poz.Y];
        }

        public bunka bunkaAt(int x, int y)
        {
            return matice[x, y];
        }

        public bool jeUvnitrBlud(int x, int y)
        {
            return ((0 <= x && x < sirka) && (0 <= y && y < vyska));
        }

        /// <summary>
        /// Zmeni rozmer bludiste v pixelech, pocet bunek a vygenrovane cesty zustanou nezmeneny.
        /// Nove bludiste do graphics nevykresli.
        /// </summary>
        /// <param name="dest">cilova graphics(s novymi rozmery), do ktere se bude bludiste kreslit</param>
        /// <param name="sirka_pix">nova sirka pixelech</param>
        /// <param name="vyska_pix">nova vyska pixelech</param>
        internal void zmenRozmer(Graphics dest, int sirka_pix, int vyska_pix)
        {
            this.graphics = dest;
            bunka.sirka = sirka_pix / sirka;
            bunka.vyska = vyska_pix / vyska;

            this.sirka_pix = sirka * bunka.sirka;
            this.vyska_pix = vyska * bunka.vyska;

            for (int i = 0; i < sirka; i++)
            {
                for (int j = 0; j < vyska; j++)
                {
                    matice[i, j].pozice = new Point(bunka.sirka * i, bunka.vyska * j);
                }
            }
        }
    
    }

    public class prisera
        {
            public Point pozice; // souradnice bunky v bludisti, kde prisera zrovna je
            bludiste doma; //bludiste, ve kterem prisera leze
            Color barva;

            public prisera(bludiste doma, Color barva, Point pozice)
            {
                this.pozice = pozice;
                this.barva = barva;
                this.doma = doma;

            }

            public void vykresliSe()
            {
                this.vykresliSe(this.barva);
            }

            private void vykresliSe(Color barva)
            {
                Brush st = new SolidBrush(barva);
                doma.graphics.FillEllipse(st, pozice.X * bunka.sirka + 1,
                    pozice.Y * bunka.vyska + 1,
                    bunka.sirka - 2, bunka.vyska - 2);
            }

            internal void jdi(int p1, int p2)
            {
                if (jeVolnaCesta(p1, p2))
                {
                    vykresliSe(Color.White);
                    pozice.X += p1;
                    pozice.Y += p2;
                    vykresliSe();

                }
            }

            private bool jeVolnaCesta(int p1, int p2)
            {
                if (!doma.jeUvnitrBlud(pozice.X + p1, pozice.Y + p2)) return false;

                if (p1 < 0) return !doma.bunkaAt(pozice).zapadni_stena;
                if (p1 > 0) return !doma.bunkaAt(pozice.X + p1, pozice.Y).zapadni_stena;
                if (p2 < 0) return !doma.bunkaAt(pozice).severni_stena;
                if (p2 > 0) return !doma.bunkaAt(pozice.X, pozice.Y + p2).severni_stena;

                return true;
            }
        }
}